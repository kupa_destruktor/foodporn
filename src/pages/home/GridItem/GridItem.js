import React from "react";
import classNames from "classnames";

let styles = require('./style.scss');
let T = React.PropTypes;

export default class GridItem extends React.Component {
  	propTypes: {
  		object: T.object,
	}

	constructor(props) {
		super(props);
		this.state = {
			clicked: false,
		};
	}

	onItemClick = () => {
		this.setState({
			clicked: !this.state.clicked,
		});
	}

	render() {
		let clicked = this.state.clicked ? styles.clicked : '';
		let descriptionClassnames = classNames(styles.description, clicked);
		let style = {
			backgroundImage: 'url(/images/upload/' + this.props.object.filename + ')',
		};
		return (
			<div className={styles.gridItem} onClick={this.onItemClick}>
				<div className={styles.hidden} >
					<div className={styles.backgroundWrapper}>
						<div className={styles.background} style={style} />
					</div>
					<div className={descriptionClassnames} >
						<div className={styles.title}>
							{this.props.object.title}
						</div>
					</div>
				</div>
			</div>
		)
	}
}