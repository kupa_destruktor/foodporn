import React from "react";
import GridItem from "./GridItem/GridItem";

require('es6-promise').polyfill();
require('isomorphic-fetch');

let styles = require('./style.scss');

var images = require("./../../../public/json/images.json");


export default class HomePage extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			items: [],
		};
	}

	signUp() {

	}

	componentDidMount() {
		let imagesArray = [];
		images.map((parts) => {
			let image = {
				id: parts[0],
				title: parts[1],
				extensions: parts[2],
				filename: parts[3],
			};
			imagesArray.push(image);
		});
      	this.setState({
      		items: imagesArray,
      	});
	}

	renderGridItems() {
		return this.state.items.map((item, index) => {
			if (index < 40)
				return <GridItem key={item.id} object={item} />
		});
	}

	render() {
		return (
			<div className={styles.content}>
        		<h1 className={styles.heading}>Food Porn</h1>
        		<p className={styles.lead}>Create an account and upload your own pictures!</p>
        		<a className={styles.signUpButton} onClick={this.signUp}>Sign up</a>
				<div className={styles.grid}>
					{this.renderGridItems()}
				</div>
			</div>
		);
	}
}
