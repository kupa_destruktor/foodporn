import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './App';
import Contact from '../../pages/contact/contact';
import LoginPage from '../../pages/login/page';
import HomePage from '../../pages/home/page';


export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage} />
    <Route path="contact" component={Contact} />
    <Route path="login" component={LoginPage} />
    <Route path='*' component={HomePage} />
  </Route>
);
