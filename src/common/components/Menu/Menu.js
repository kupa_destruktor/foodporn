import React from "react";
import {Link, browserHistory} from 'react-router';

let styles = require('./style.scss');
let T = React.PropTypes;

export default class Menu extends React.Component {
	render() {
		return (
			<div className={styles.menu}>
				<ul className={styles.menuList}>
					<li><Link to={"/"}>Homepage</Link></li>
					<li><Link to={"contact"}>Contact</Link></li>
				</ul>
			</div>
		)
	}
}